#!/bin/bash

ESC_SEQ="\x1b["
COL_RESET=$ESC_SEQ"39;49;00m"
COL_RED=$ESC_SEQ"31;01m"
COL_GREEN=$ESC_SEQ"32;01m"
COL_YELLOW=$ESC_SEQ"33;01m"

if [ "$UID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

function error_check {
    if [ "$?" = "0" ]; then
        echo -e "$COL_GREEN OK. $COL_RESET"
    else
        echo -e "$COL_RED An error has occured. $COL_RESET"
		
        read -p "Press enter or space to ignore it. Press any other key to abort." -n 1 key

        if [[ $key != "" ]]; then
            exit
        fi
    fi
}

read -p "Enter domain:" domain

echo "Updating system"
apt-get update
apt-get -y upgrade

echo "Installing programs"
apt-get install -y curl openssh-server ca-certificates
apt-get install -y postfix
curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | bash
EXTERNAL_URL="http://$domain" apt-get -y install gitlab-ee
error_check

echo "Creating gitlab config file (/etc/gitlab/gitlab.rb)"
echo "letsencrypt['enable'] = true
letsencrypt['auto_renew'] = true
letsencrypt['auto_renew_hour'] = \"10\"
letsencrypt['auto_renew_minute'] = \"00\"
letsencrypt['auto_renew_day_of_month'] = \"*/7\"
external_url \"https://$domain\"
nginx['redirect_http_to_https'] = true
gitlab_rails['time_zone'] = 'Europe/Berlin'
gitlab_rails['smtp_enable'] = true
gitlab_rails['smtp_address'] = \"mail.gmx.com\"
gitlab_rails['smtp_port'] = 465
gitlab_rails['smtp_user_name'] = \"vitaeterna.spdns.org@gmx.com\"
gitlab_rails['smtp_password'] = \"vitaeterna.spdns.org!!\"
gitlab_rails['smtp_domain'] = \"mail.gmx.com\"
gitlab_rails['smtp_authentication'] = \"login\"
gitlab_rails['smtp_enable_starttls_auto'] = true
gitlab_rails['smtp_tls'] = true
gitlab_rails['smtp_openssl_verify_mode'] = 'peer'
gitlab_rails['gitlab_email_enabled'] = true
gitlab_rails['gitlab_email_from'] = 'vitaeterna.spdns.org@gmx.com'
gitlab_rails['gitlab_email_display_name'] = 'GitLab@vitaeterna.spdns.org'
gitlab_rails['gitlab_email_reply_to'] = 'vitaeterna.spdns.org@gmx.com'" > /etc/gitlab/gitlab.rb
error_check

gitlab-ctl reconfigure
gitlab-ctl restart